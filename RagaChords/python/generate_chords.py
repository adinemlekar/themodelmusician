SWARAS = ['S', 'r', 'R', 'g', 'G', 'm', 'M', 'P', 'd', 'D', 'n', 'N']
THAATS = {
	"Asavari": [0, 2, 3, 5, 7, 8, 10],
	"Bhairav": [0, 1, 4, 5, 7, 8, 10],
	"Bhairavi": [0, 1, 3, 5, 7, 8, 10],
	"Bilawal": [0, 2, 4, 5, 7, 9, 11],
	"Kafi": [0, 2, 3, 5, 7, 9, 10],
	"Kalyan": [0, 2, 4, 6, 7, 9, 11],
	"Khamaj": [0, 2, 4, 5, 7, 9, 10],
	'Marwa': [0, 1, 4, 6, 7, 9, 11],
	'Poorvi': [0, 1, 4, 6, 7, 8, 11],
	"Todi": [0, 1, 3, 6, 7, 8, 11],
}

NOTES = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
TRIAD_TYPES = {
	"Maj": [0, 4, 7], # 1-3-5
	"Min": [0, 3, 7], # 1-3b-5
	"Aug": [0, 4, 8], # 1-3-5#
	"Dim": [0, 3, 6], # 1-3b-5b
	"Half-Dim": [0, 4, 6], # 1-3-5b (and 7b technically)
	"Sus2": [0, 2, 7], # 1-2-5
	"Sus4": [0, 5, 7], # 1-4-5
}

CONSOLE_OUTPUT = True
CHORD_DATABASE_LINK = "http://www.chordsdatabase.com/guitar/"


def get_swaras_in_thaat(thaat):
	if thaat not in THAATS:
		raise ValueError("Unrecorgnized thaat : {0}".format(thaat))

	note_indexes = THAATS[thaat]
	return [SWARAS[index] for index in note_indexes]

def get_notes_in_thaat(thaat):
	if thaat not in THAATS:
		raise ValueError("Unrecorgnized thaat : {0}".format(thaat))

	note_indexes = THAATS[thaat]
	return [NOTES[index] for index in note_indexes]

def get_triads_from_swaras(swaras):
	triads = []
	for i in range(len(swaras)):
		triad = [swaras[i], swaras[(i+2) % len(swaras)], swaras[(i+4) % len(swaras)]]
		triads.append(triad)
	return triads

def get_triad_types_from_triads(triads):
	triad_types = []
	for i, triad in enumerate(triads):
		triad_indexes = [SWARAS.index(swar) for swar in triad]
		triad_offsets = [((index - triad_indexes[0]) % len(SWARAS)) for index in triad_indexes]
		triad_type = None
		for ttype, toffset in TRIAD_TYPES.items():
			if toffset == triad_offsets:
				triad_type = ttype
		triad_types.append((triad, triad_type if triad_type else "** Unfamiliar intervals **"))
	return triad_types

def get_chord_types_for_thaat(thaat):
	thaat_swaras = get_swaras_in_thaat(thaat)
	thaat_triads = get_triads_from_swaras(thaat_swaras)
	thaat_triad_types = get_triad_types_from_triads(thaat_triads)

	return thaat_triad_types

def transpose_notes_to_key(notes, key):
	if key not in NOTES:
		raise ValueError("Unrecognized key: {0}".format(key))

	offset = NOTES.index(key)
	return [NOTES[(NOTES.index(note) + offset) % len(NOTES)] for note in notes]

def get_chords_for_thaat_in_key(thaat, transpose_key):
	notes_for_thaat_in_C = get_notes_in_thaat(thaat)
	transposed_notes = transpose_notes_to_key(notes_for_thaat_in_C, transpose_key)
	thaat_triad_types = get_chord_types_for_thaat(thaat)

	chords_for_thaat = []
	for i, (swaras_in_triad, triad_type) in enumerate(thaat_triad_types):
		chords_for_thaat.append((swaras_in_triad, transposed_notes[i], triad_type))
	return chords_for_thaat

def get_all_info_for_thaat_key(thaat, transpose_key):
	notes_for_thaat_in_C = get_notes_in_thaat(thaat)
	transposed_notes = transpose_notes_to_key(notes_for_thaat_in_C, transpose_key)
	thaat_triad_types = get_chord_types_for_thaat(thaat)

	chords_in_thaat = get_chords_for_thaat_in_key(thaat, transpose_key)
	return chords_in_thaat

def output_chords_in_key(transpose_key):
	# Write output to CSV file.
	with open('chords_in_{0}.csv'.format(transpose_key), 'w') as output_file:
		for thaat in THAATS.keys():
			chords_in_thaat = get_all_info_for_thaat_key(thaat, transpose_key)
			
			if CONSOLE_OUTPUT:
				print("--- {0} ---".format(thaat.upper()))
			output_file.write(thaat.upper() + "\n")
			output_file.write("SWARA TRIAD, CHORD NAME, GUITAR CHORD SHAPES\n")
			
			for swaras_in_triad, note, triad_type in chords_in_thaat:
				if CONSOLE_OUTPUT:
					print("{0} --> {1}-{2}".format(' '.join(swaras_in_triad), note, triad_type))
				
				# Construct chord-database hyperlink.
				chord_link = ''
				if 'Maj' in triad_type:
					chord_link = '{0}{1}-chord/'.format(CHORD_DATABASE_LINK, note)
				elif 'Min' in triad_type:
					chord_link = '{0}{1}m-chord/'.format(CHORD_DATABASE_LINK, note)
				elif 'Aug' in triad_type:
					chord_link = '{0}{1}+-chord/'.format(CHORD_DATABASE_LINK, note)
				elif 'Dim' in triad_type:
					chord_link = '{0}{1}°-chord/'.format(CHORD_DATABASE_LINK, note)
				elif 'Sus2' in triad_type:
					chord_link = '{0}{1}sus2-chord/'.format(CHORD_DATABASE_LINK, note)
				elif 'Sus4' in triad_type:
					chord_link = '{0}{1}sus4-chord/'.format(CHORD_DATABASE_LINK, note)
				else:
					chord_link = "Unavailable."

				output_file.write("{0}, {1}, {2}\n".format(' '.join(swaras_in_triad), '{0}{1}'.format(note, triad_type), chord_link))

			if CONSOLE_OUTPUT:
				print("\n")
			output_file.write("\n")
			output_file.write("\n")


def main():
	for transpose_key in NOTES:
		output_chords_in_key(transpose_key)

if __name__ == "__main__":
	main()
