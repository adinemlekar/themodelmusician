
var keySelectorElem, 
	thaatSelectorElem,
	thaatSwarasElem,
	thaatTriadsElem,
	thaatChordsElem,
	notesInKey

window.onload = function() {
	keySelectorElem = document.getElementById( "key_selector" );
	thaatSelectorElem = document.getElementById("thaat_selector");
	thaatSwarasElem = document.getElementById("thaat_swaras");
	thaatTriadsElem = document.getElementById("thaat_triads");
	thaatChordsElem = document.getElementById("thaat_chords");
	notesInKeyElem = document.getElementById("notes_in_key");
}

const SWARAS = ['S', 'r', 'R', 'g', 'G', 'm', 'M', 'P', 'd', 'D', 'n', 'N']
const THAATS =  {
	"Asavari": [0, 2, 3, 5, 7, 8, 10],
	"Bhairav": [0, 1, 4, 5, 7, 8, 10],
	"Bhairavi": [0, 1, 3, 5, 7, 8, 10],
	"Bilawal": [0, 2, 4, 5, 7, 9, 11],
	"Kafi": [0, 2, 3, 5, 7, 9, 10],
	"Kalyan": [0, 2, 4, 6, 7, 9, 11],
	"Khamaj": [0, 2, 4, 5, 7, 9, 10],
	'Marwa': [0, 1, 4, 6, 7, 9, 11],
	'Poorvi': [0, 1, 4, 6, 7, 8, 11],
	"Todi": [0, 1, 3, 6, 7, 8, 11],
}
const NOTES =  ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
const TRIAD_TYPES = {
	"Maj": [0, 4, 7], // 1-3-5
	"Min": [0, 3, 7], // 1-3b-5
	"Aug": [0, 4, 8], // 1-3-5#
	"Dim": [0, 3, 6], // 1-3b-5b
	"Half-Dim": [0, 4, 6], // 1-3-5b (and 7b technically)
	"Sus2": [0, 2, 7], // 1-2-5
	"Sus4": [0, 5, 7], // 1-4-5
}

function getSwarasInThaat(thaat) {
	if(!(thaat in THAATS)) {
		throw "Unrecognized Thaat";
	}

	var noteIndexes = THAATS[thaat];
	var swarasInThaat = [];
	for(var i = 0; i < noteIndexes.length; i++){
		swarasInThaat.push(SWARAS[noteIndexes[i]]);
	}
	
	return swarasInThaat
}

function getNotesInThaat(thaat) {
	if(!(thaat in THAATS)) {
		throw "Unrecognized Thaat";
	}

	var noteIndexes = THAATS[thaat];
	var notesInThaat = [];
	for(var i = 0; i < noteIndexes.length; i++){
		notesInThaat.push(NOTES[noteIndexes[i]]);
	}
	
	return notesInThaat;
}

function getTriadsFromSwaras(swaras) {
	var triads = [];
	for (var i = 0; i < swaras.length; i++) {
		var triad = [swaras[i], swaras[(i+2) % swaras.length], swaras[(i+4) % swaras.length]];
			triads.push(triad);
	}
	return triads;
}

function areListsEqual(l1, l2) {
	if (l1.length != l2.length) 
		return false;

	for(var i = 0; i < l1.length; i++)
		if(l1[i] != l2[i])
			return false;

	return true;
}

function getTriadTypesFromTriads(triads) {
	triadTypes = [];
	for(var i = 0; i  < triads.length; i++){
		triad = triads[i];

		triadIndexes = [];
		for(var j = 0; j < triad.length; j++) {
			triadIndexes.push(SWARAS.indexOf(triad[j]));
		}

		triadOffsets = [];
		for(var j = 0; j < triadIndexes.length; j++) {
			offset = (triadIndexes[j] - triadIndexes[0]) % SWARAS.length;
			if(offset < 0)
				offset = SWARAS.length + offset
			triadOffsets.push(offset);
		}

		triadType = null;
		for(var ttype in TRIAD_TYPES) {
			toffset = TRIAD_TYPES[ttype];
			if(areListsEqual(toffset, triadOffsets))
				triadType = ttype;
		}
		if(triadType == null) 
			triadTypes.push([triad, "** Unfamiliar Intervals **"]);
		else
			triadTypes.push([triad, triadType])
	}

	return triadTypes;
}

function getChordTypesForThaat(thaat) {
	thaatSwaras = getSwarasInThaat(thaat);
	thaatTriads = getTriadsFromSwaras(thaatSwaras);
	thaatTriadTypes = getTriadTypesFromTriads(thaatTriads);

	return thaatTriadTypes;
}

function transposeNotesToKey(notes, key) {
	offset = NOTES.indexOf(key);
	transposedNotes = [];
	for(var i = 0; i < notes.length; i++){
		note = notes[i];
		transposedNote = NOTES[(NOTES.indexOf(note) + offset) % NOTES.length];
		transposedNotes.push(transposedNote);
	}

	return transposedNotes;
}

function getChordsForThaat(transposedNotes, thaat) {
	triads = getChordTypesForThaat(thaat)

	chordsForThaat = []
	for(var i = 0; i < triads.length; i++) {
		triadInfo = triads[i];
		triad = triadInfo[0]
		triadType = triadInfo[1];

		chordsForThaat.push([triad, transposedNotes[i], triadType])
	}

	return chordsForThaat;
}

function prettyPrintSwaras(swaras){
	swaraStr = "";
	for(var i = 0; i < swaras.length; i++) {
		swaraStr = swaraStr.concat(swaras[i] + " ");
	}
	return swaraStr;
}

function prettyPrintResultTriads(results){
	resultStr = "";
	for(var i =0; i < results.length; i++) {
		result = results[i]
		resultStr = resultStr.concat(result[0] + "\n")
	}
	return resultStr;
}

function prettyPrintResultChords(results){
	resultStr = "";
	for(var i =0; i < results.length; i++) {
		result = results[i]
		resultStr = resultStr.concat(result[1] + "-" + result[2] + "\n")
	}
	return resultStr;
}

function generateChords() {
	var transposeKey = keySelectorElem.value;
	var thaat = thaatSelectorElem.value;

	swarasInThaat = getSwarasInThaat(thaat);
	notesInThaat = getNotesInThaat(thaat);
	transposedNotes = transposeNotesToKey(notesInThaat, transposeKey)
	results = getChordsForThaat(transposedNotes, thaat)

	thaatSwarasElem.value = prettyPrintSwaras(swarasInThaat);
	notesInKeyElem.value = prettyPrintSwaras(transposedNotes);
	thaatTriadsElem.value = prettyPrintResultTriads(results);
	thaatChordsElem.value = prettyPrintResultChords(results);
}